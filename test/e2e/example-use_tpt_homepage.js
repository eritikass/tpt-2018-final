var config = require('../../nightwatch.conf.js');

module.exports = {
    'tpt test': function(browser) {
        browser
            .url('http://www.tptlive.ee')
            .waitForElementVisible('#main-menu-wrapper', 1000)
            .saveScreenshot(config.imgpath(browser) + 'tptlive.png')
            .click('#menu-item-1313')
            .pause(1000)
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png')
            .useXpath()
            .click('//a[text()=\'TA-17E\']')
            .useCss()
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'ta-17e.png')
            .pause(3000)
            .end();

    }
};